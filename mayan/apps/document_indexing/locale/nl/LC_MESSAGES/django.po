# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Evelijn Saaltink <evelijnsaaltink@gmail.com>, 2016
# j jacobs <j.jacobs@rijnlandslyceum-rls.nl>, 2020
# Johan Braeken, 2017
# Lucas Weel <ljj.weel@gmail.com>, 2013
msgid ""
msgstr ""
"Project-Id-Version: Mayan EDMS\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-27 23:34-0400\n"
"PO-Revision-Date: 2020-04-27 14:54+0000\n"
"Last-Translator: j jacobs <j.jacobs@rijnlandslyceum-rls.nl>\n"
"Language-Team: Dutch (http://www.transifex.com/rosarior/mayan-edms/language/nl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: nl\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: admin.py:22
msgid "None"
msgstr "Geen"

#: admin.py:24 links.py:93 models.py:52
msgid "Document types"
msgstr "Documenttypen"

#: apps.py:54 events.py:6
msgid "Document indexing"
msgstr "Documentindexering"

#: apps.py:128
msgid "Total levels"
msgstr "Totaal aantal niveaus"

#: apps.py:135
msgid "Total documents"
msgstr "Totaal aantal documenten"

#: apps.py:141 apps.py:155 apps.py:180 apps.py:190
msgid "Level"
msgstr "Niveau"

#: apps.py:160 apps.py:195
msgid "Levels"
msgstr "Niveaus"

#: apps.py:167 apps.py:175 apps.py:203 models.py:378
msgid "Documents"
msgstr "Documenten"

#: events.py:10
msgid "Index created"
msgstr "Index aangemaakt"

#: events.py:13
msgid "Index edited"
msgstr "Index bewerkt"

#: forms.py:16
msgid "Index templates to be queued for rebuilding."
msgstr ""

#: forms.py:17 links.py:27
msgid "Index templates"
msgstr ""

#: forms.py:32
msgid "Template"
msgstr "Sjabloon"

#: handlers.py:18
msgid "Creation date"
msgstr "Aanmaakdatum"

#: links.py:21 links.py:36 links.py:73 links.py:77 models.py:60 views.py:150
#: views.py:333
msgid "Indexes"
msgstr "Indexeringen"

#: links.py:42 queues.py:22
msgid "Rebuild index"
msgstr "Index opnieuw opbouwen"

#: links.py:51 links.py:62
msgid "Deletes and creates from scratch all the document indexes."
msgstr "Alle documentindexeringen vanaf nul opbouwen"

#: links.py:53 views.py:446
msgid "Rebuild indexes"
msgstr "Opnieuw indexeren"

#: links.py:64 views.py:481
msgid "Reset indexes"
msgstr "Indexering opnieuw instellen"

#: links.py:81 views.py:86
msgid "Create index"
msgstr "Indexering aanmaken"

#: links.py:88 links.py:118
msgid "Delete"
msgstr "Verwijderen"

#: links.py:99 links.py:123
msgid "Edit"
msgstr "Bewerken"

#: links.py:106
msgid "Tree template"
msgstr "Sjabloon boomstructuur"

#: links.py:112
msgid "New child node"
msgstr "Nieuwe node"

#: models.py:34
msgid "Short description of this index."
msgstr "Korte beschrijving van deze index."

#: models.py:35
msgid "Label"
msgstr "Label"

#: models.py:39
msgid "This value will be used by other apps to reference this index."
msgstr ""

#: models.py:40
msgid "Slug"
msgstr ""

#: models.py:45
msgid ""
"Causes this index to be visible and updated when document data changes."
msgstr "Maakt deze index zichtbaar en 'up-to-date' wanneer documentgegevens wijzigen."

#: models.py:48 models.py:253
msgid "Enabled"
msgstr "Ingeschakeld"

#: models.py:59 models.py:237
msgid "Index"
msgstr "Index"

#: models.py:209
msgid "Index instance"
msgstr "index instance"

#: models.py:210
msgid "Index instances"
msgstr ""

#: models.py:241
msgid ""
"Enter a template to render. Use Django's default templating language "
"(https://docs.djangoproject.com/en/1.11/ref/templates/builtins/)"
msgstr ""

#: models.py:245
msgid "Indexing expression"
msgstr "Indexeringsexpressie"

#: models.py:250
msgid "Causes this node to be visible and updated when document data changes."
msgstr "Maakt deze node zichtbaar en 'up-to-date' wanneer document gegevens wijzigen"

#: models.py:258
msgid ""
"Check this option to have this node act as a container for documents and not"
" as a parent for further nodes."
msgstr "Selecteer deze optie, wanneer deze node alleen documenten dient te bevatten. "

#: models.py:261
msgid "Link documents"
msgstr "Koppel documenten"

#: models.py:265
msgid "Index node template"
msgstr ""

#: models.py:266
msgid "Indexes node template"
msgstr ""

#: models.py:270
msgid "Root"
msgstr ""

#: models.py:325
#, python-format
msgid ""
"Error indexing document: %(document)s; expression: %(expression)s; "
"%(exception)s"
msgstr "Fout bij het indexeren van document: %(document)s; uitdrukking: %(expression)s; %(exception)s"

#: models.py:371
msgid "Index template node"
msgstr ""

#: models.py:374 search.py:23
msgid "Value"
msgstr "Waarde"

#: models.py:384 models.py:510
msgid "Index instance node"
msgstr ""

#: models.py:385
msgid "Indexes instances node"
msgstr ""

#: models.py:447
msgid "The path to the index including all ancestors."
msgstr ""

#: models.py:449
msgid "Full path"
msgstr "Volledig pad"

#: models.py:503
msgid "Document index node instance"
msgstr ""

#: models.py:504
msgid "Document indexes node instances"
msgstr ""

#: models.py:511
msgid "Index instance nodes"
msgstr ""

#: permissions.py:5 queues.py:7
msgid "Indexing"
msgstr "Indexering bezig"

#: permissions.py:8
msgid "Create new document indexes"
msgstr "Nieuw document-indexeringen aanmaken"

#: permissions.py:11
msgid "Edit document indexes"
msgstr "Document-indexeringen bewerken"

#: permissions.py:14
msgid "Delete document indexes"
msgstr "Document-indexeringen verwijderen"

#: permissions.py:17
msgid "View document index instances"
msgstr ""

#: permissions.py:21
msgid "View document indexes"
msgstr "Document-indexeringen bekijken"

#: permissions.py:24
msgid "Rebuild document indexes"
msgstr "Document-indexeringen opnieuw opbouwen"

#: queues.py:10
msgid "Delete empty index nodes"
msgstr ""

#: queues.py:14
msgid "Remove document"
msgstr "Document verwijderen"

#: queues.py:18
msgid "Index document"
msgstr "Document indexeren"

#: search.py:27
msgid "Document type"
msgstr "Documenttype"

#: search.py:30
msgid "Document MIME type"
msgstr "Document MIME type"

#: search.py:33
msgid "Document label"
msgstr "Documentlabel"

#: search.py:36
msgid "Document description"
msgstr "Documentbeschrijving"

#: search.py:39
msgid "Document UUID"
msgstr "Document UUID"

#: search.py:43
msgid "Document checksum"
msgstr "Document checksum"

#: views.py:45
msgid "Available indexes"
msgstr "Beschikbare indexingen"

#: views.py:46
msgid "Indexes linked"
msgstr "Indexeringen gekoppeld"

#: views.py:76
msgid ""
"Documents of this type will appear in the indexes linked when these are "
"updated. Events of the documents of this type will trigger updates in the "
"linked indexes."
msgstr ""

#: views.py:80
#, python-format
msgid "Indexes linked to document type: %s"
msgstr "Indexeringen gekoppeld aan documenttype: %s"

#: views.py:109
#, python-format
msgid "Delete the index: %s?"
msgstr "Verwijder de index: %s?"

#: views.py:125
#, python-format
msgid "Edit index: %s"
msgstr "Bewerk index: %s"

#: views.py:144
msgid ""
"Indexes group document automatically into levels. Indexe are defined using "
"template whose markers are replaced with direct properties of documents like"
" label or description, or that of extended properties like metadata."
msgstr ""

#: views.py:149
msgid "There are no indexes."
msgstr "Er zijn geen indexeringen."

#: views.py:162
#, python-format
msgid "Rebuild index: %s"
msgstr "Opnieuw opbouwen index: %s"

#: views.py:182
msgid "Index queued for rebuild."
msgstr "Index in wachtrij voor opnieuw opbouwen."

#: views.py:194
msgid "Available document types"
msgstr "Beschikbare documenttypen"

#: views.py:195
msgid "Document types linked"
msgstr "Documenttypen gekoppeld"

#: views.py:205
msgid ""
"Only the documents of the types selected will be shown in the index when "
"built. Only the events of the documents of the types select will trigger "
"updates in the index."
msgstr ""

#: views.py:209
#, python-format
msgid "Document types linked to index: %s"
msgstr "Documenttypen gekoppeld aan index: %s"

#: views.py:226
#, python-format
msgid "Tree template nodes for index: %s"
msgstr ""

#: views.py:254
#, python-format
msgid "Create child node of: %s"
msgstr ""

#: views.py:280
#, python-format
msgid "Delete the index template node: %s?"
msgstr ""

#: views.py:304
#, python-format
msgid "Edit the index template node: %s?"
msgstr ""

#: views.py:328
msgid ""
"This could mean that no index templates have been created or that there "
"index templates but they are no properly defined."
msgstr ""

#: views.py:332
msgid "There are no index instances available."
msgstr "Er zijn geen index-instanties beschikbaar."

#: views.py:377
#, python-format
msgid "Navigation: %s"
msgstr "Navigatie: %s"

#: views.py:382
#, python-format
msgid "Contents for index: %s"
msgstr "Inhoud voor index: %s"

#: views.py:425
msgid ""
"Assign the document type of this document to an index to have it appear in "
"instances of those indexes organization units. "
msgstr ""

#: views.py:430
msgid "This document is not in any index"
msgstr "Dit document is in geen enkele index"

#: views.py:434
#, python-format
msgid "Indexes nodes containing document: %s"
msgstr ""

#: views.py:460
#, python-format
msgid "%(count)d index queued for rebuild."
msgid_plural "%(count)d indexes queued for rebuild."
msgstr[0] "%(count)d index in wachtrij voor opnieuw opbouwen."
msgstr[1] "%(count)d indexeringen in wachtrij voor opnieuw opbouwen."

#: views.py:493
#, python-format
msgid "%(count)d index reset."
msgid_plural "%(count)d indexes reset."
msgstr[0] "%(count)d index opnieuw ingesteld."
msgstr[1] "%(count)d indexeringen opnieuw ingesteld."

#: views.py:506
msgid "Index templates for which their instances will be deleted."
msgstr ""
